# Translation of kmahjongg.po to Brazilian Portuguese
# Copyright (C) 2002-2019 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Lisiane Sztoltz <lisiane@conectiva.com.br>, 2002, 2003.
# Lisiane Sztoltz Teixeira <lisiane@conectiva.com.br>, 2004.
# Thiago Macieira <thiago.macieira@kdemail.net>, 2004.
# Anderson Carlos Daniel Sanches <anderson@ime.usp.br>, 2004.
# Stephen Killing <stephen.killing@kdemail.net>, 2005.
# Mauricio Piacentini <piacentini@kde.org>, 2007.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2009, 2010, 2011, 2012, 2016, 2019.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2009, 2012, 2017, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kmahjongg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-27 00:55+0000\n"
"PO-Revision-Date: 2023-02-07 10:45-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""
"Lisiane Sztoltz, João Emanuel, Anderson Sanches, Stephen Killing, Mauricio "
"Piacentini, André Marcelo Alvarenga"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"lisiane@conectiva.com.br, joaoemanuel@terra.com.br, anderson@ime.usp.br, "
"stephen.killing@kdemail.net, piacentini@kde.org, alvarenga@kde.org"

#: src/editor.cpp:72
#, kde-format
msgctxt "@title:window"
msgid "Edit Board Layout"
msgstr "Editar o formato do tabuleiro"

#: src/editor.cpp:112
#, kde-format
msgid "New board"
msgstr "Novo tabuleiro"

#: src/editor.cpp:119
#, kde-format
msgid "Open board"
msgstr "Abrir tabuleiro"

#: src/editor.cpp:126
#, kde-format
msgid "Save board"
msgstr "Salvar tabuleiro"

#: src/editor.cpp:136
#, kde-format
msgid "Select"
msgstr "Selecionar"

#: src/editor.cpp:141
#, kde-format
msgid "Cut"
msgstr "Recortar"

#: src/editor.cpp:146
#, kde-format
msgid "Copy"
msgstr "Copiar"

#: src/editor.cpp:151
#, kde-format
msgid "Paste"
msgstr "Colar"

#: src/editor.cpp:158
#, kde-format
msgid "Move tiles"
msgstr "Mover peças"

#: src/editor.cpp:162
#, kde-format
msgid "Add tiles"
msgstr "Adicionar peças"

#: src/editor.cpp:165
#, kde-format
msgid "Remove tiles"
msgstr "Remover peças"

#: src/editor.cpp:189
#, kde-format
msgid "Shift left"
msgstr "Deslocar para esquerda"

#: src/editor.cpp:195
#, kde-format
msgid "Shift up"
msgstr "Deslocar para cima"

#: src/editor.cpp:201
#, kde-format
msgid "Shift down"
msgstr "Deslocar para baixo"

#: src/editor.cpp:207
#, kde-format
msgid "Shift right"
msgstr "Deslocar para direita"

#: src/editor.cpp:289
#, kde-format
msgid "Tiles: %1 Pos: %2,%3,%4"
msgstr "Peças: %1 Pos: %2,%3,%4"

#: src/editor.cpp:298
#, kde-format
msgid "Open Board Layout"
msgstr "Abrir formato do tabuleiro"

#: src/editor.cpp:299 src/editor.cpp:337
#, kde-format
msgid "Board Layout (*.layout);;All Files (*)"
msgstr "Layout de tabuleiro (*.layout);;Todos os arquivos (*)"

#: src/editor.cpp:330
#, kde-format
msgid "You can only save with a even number of tiles."
msgstr "Você só pode salvar com um número par de peças."

#: src/editor.cpp:336 src/editor.cpp:348
#, kde-format
msgid "Save Board Layout"
msgstr "Salvar formato do tabuleiro"

#: src/editor.cpp:347
#, kde-format
msgid "A file with that name already exists. Do you wish to overwrite it?"
msgstr "Já existe um arquivo com este nome. Deseja sobrescrevê-lo?"

#: src/editor.cpp:380
#, kde-format
msgid "The board has been modified. Would you like to save the changes?"
msgstr "O tabuleiro foi modificado. Gostaria de salvar as alterações?"

#: src/editor.cpp:392
#, kde-format
msgid "Save failed. Aborting operation."
msgstr "O salvamento falhou. Cancelando a operação."

#: src/gameremovedtiles.cpp:127
#, kde-format
msgid "Removed tiles"
msgstr "Peças removidas"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_RandomLayout)
#: src/gametype.ui:25
#, kde-format
msgid "Random Layout"
msgstr "Disposição aleatória"

#. i18n: ectx: property (text), widget (QPushButton, getNewButton)
#: src/gametype.ui:51
#, kde-format
msgid "&Get New Layouts"
msgstr "&Baixar novos formatos de tabuleiro"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: src/gametype.ui:60
#, kde-format
msgid "Preview"
msgstr "Visualização"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: src/gametype.ui:93
#, kde-format
msgid "Layout Details"
msgstr "Detalhes do formato"

#. i18n: ectx: property (text), widget (QLabel, labelAuthor)
#: src/gametype.ui:113
#, kde-format
msgid "Author:"
msgstr "Autor:"

#. i18n: ectx: property (text), widget (QLabel, labelContact)
#: src/gametype.ui:123
#, kde-format
msgid "Contact:"
msgstr "Contato:"

#. i18n: ectx: property (text), widget (QLabel, labelDescription)
#: src/gametype.ui:133
#, kde-format
msgid "Description:"
msgstr "Descrição:"

#: src/gameview.cpp:135 src/gameview.cpp:246
#, kde-format
msgid "Ready. Now it is your turn."
msgstr "Pronto. Agora é a sua vez."

#: src/gameview.cpp:157
#, kde-format
msgid "Undo operation done successfully."
msgstr "Operação de desfazer concluída com sucesso."

#: src/gameview.cpp:162
#, kde-format
msgid "What do you want to undo? You have done nothing!"
msgstr "O que deseja desfazer? Você não fez nada!"

#: src/gameview.cpp:196 src/kmahjongg.cpp:486
#, kde-format
msgid "Your computer has lost the game."
msgstr "Seu computador perdeu o jogo."

#: src/gameview.cpp:203
#, kde-format
msgid "Calculating new game..."
msgstr "Calculando novo jogo..."

#: src/gameview.cpp:261
#, kde-format
msgid "Error generating new game!"
msgstr "Ocorreu um erro ao gerar o novo jogo!"

#: src/gameview.cpp:366
#, kde-format
msgid "Demo mode. Click mousebutton to stop."
msgstr "Modo de demonstração. Clique no botão do mouse para parar."

#: src/kmahjongg.cpp:141
#, kde-format
msgid "New Numbered Game..."
msgstr "Novo jogo numerado..."

#: src/kmahjongg.cpp:148
#, kde-format
msgid "Shu&ffle"
msgstr "Em&baralhar"

#: src/kmahjongg.cpp:153
#, kde-format
msgid "Rotate View Counterclockwise"
msgstr "Girar no sentido anti-horário"

#: src/kmahjongg.cpp:159
#, kde-format
msgid "Rotate View Clockwise"
msgstr "Girar no sentido horário"

#: src/kmahjongg.cpp:179
#, kde-format
msgid "&Board Editor"
msgstr "&Editor de tabuleiro"

#: src/kmahjongg.cpp:198
#, kde-format
msgid "Time: 0:00:00"
msgstr "Tempo: 0:00:00"

#: src/kmahjongg.cpp:205
#, kde-format
msgid "Removed: 0000/0000"
msgstr "Removidas: 0000/0000"

#: src/kmahjongg.cpp:212
#, kde-format
msgid "Game: 000000000000000000000"
msgstr "Jogo: 000000000000000000000"

#: src/kmahjongg.cpp:225
#, kde-format
msgid "Time: "
msgstr "Tempo: "

#: src/kmahjongg.cpp:231
#, kde-format
msgctxt "@title:window"
msgid "New Game"
msgstr "Novo jogo"

#: src/kmahjongg.cpp:231
#, kde-format
msgid "Enter game number:"
msgstr "Digite o número do jogo:"

#: src/kmahjongg.cpp:266
#, kde-format
msgid "General"
msgstr "Geral"

#: src/kmahjongg.cpp:267
#, kde-format
msgid "Board Layout"
msgstr "Formato do tabuleiro"

#: src/kmahjongg.cpp:408
#, kde-format
msgid "Game Over: You have no moves left."
msgstr "Fim do jogo: Você não tem mais movimentos."

#: src/kmahjongg.cpp:409
#, kde-format
msgid "Game Over"
msgstr "Fim do jogo"

#: src/kmahjongg.cpp:410
#, kde-format
msgid "New Game"
msgstr "Novo jogo"

#: src/kmahjongg.cpp:411
#, kde-format
msgid "Restart"
msgstr "Reiniciar"

#: src/kmahjongg.cpp:539
#, kde-format
msgid "You have won with a final time of %1 and a score of %2!"
msgstr "Você ganhou com um tempo final de %1 e uma pontuação de %2!"

#: src/kmahjongg.cpp:564
#, kde-format
msgid "Game number: %1"
msgstr "Número do jogo: %1"

#: src/kmahjongg.cpp:574
#, kde-format
msgid "Removed: %1/%2  Combinations left: %3"
msgstr "Removidas: %1/%2 Pares restantes: %3"

#: src/kmahjongg.cpp:635
#, kde-format
msgctxt "@title:window"
msgid "Load Game"
msgstr "Carregar jogo"

#: src/kmahjongg.cpp:635 src/kmahjongg.cpp:710
#, kde-format
msgid "KMahjongg Game (*.kmgame)"
msgstr "Partida do KMahjongg (*.kmgame)"

#: src/kmahjongg.cpp:644
#, kde-format
msgid "Could not read from file. Aborting."
msgstr "Não foi possível ler o arquivo. Finalizando."

#: src/kmahjongg.cpp:655
#, kde-format
msgid "File is not a KMahjongg game."
msgstr "O arquivo não é um jogo de KMahjongg."

#: src/kmahjongg.cpp:668
#, kde-format
msgid "File format not recognized."
msgstr "Formato de arquivo desconhecido."

#: src/kmahjongg.cpp:710
#, kde-format
msgctxt "@title:window"
msgid "Save Game"
msgstr "Salvar jogo"

#: src/kmahjongg.cpp:720
#, kde-format
msgid "Could not open file for saving."
msgstr "Não foi possível abrir o arquivo para salvá-lo."

#: src/kmahjongg.cpp:760
#, kde-format
msgid "Do you want to save your game?"
msgstr "Deseja salvar seu jogo?"

#: src/kmahjongg.cpp:760
#, kde-format
msgid "Save game?"
msgstr "Salvar jogo?"

#. i18n: ectx: label, entry (TileSet), group (General)
#: src/kmahjongg.kcfg:9
#, kde-format
msgid "The tile-set to use."
msgstr "O conjunto de peças a ser usado."

#. i18n: ectx: label, entry (Background), group (General)
#: src/kmahjongg.kcfg:12
#, kde-format
msgid "The background to use."
msgstr "O plano de fundo a ser usado."

#. i18n: ectx: label, entry (Layout), group (General)
#: src/kmahjongg.kcfg:15
#, kde-format
msgid "The layout of the tiles."
msgstr "A disposição das peças."

#. i18n: ectx: label, entry (RemovedTiles), group (General)
#: src/kmahjongg.kcfg:21
#, kde-format
msgid "Whether the removed tiles will be shown on the board."
msgstr "Se as peças removidas serão mostradas no tabuleiro."

#. i18n: ectx: label, entry (RandomLayout), group (General)
#: src/kmahjongg.kcfg:25
#, kde-format
msgid "Whether a random layout is chosen on startup."
msgstr "Se é escolhida uma disposição aleatória ao iniciar."

#. i18n: ectx: label, entry (SolvableGames), group (General)
#: src/kmahjongg.kcfg:29
#, kde-format
msgid "Whether all games should be solvable."
msgstr "Se todos os jogos devem ter solução possível."

#. i18n: ectx: label, entry (ShowMatchingTiles), group (General)
#: src/kmahjongg.kcfg:33
#, kde-format
msgid "Whether matching tiles are shown."
msgstr "Se as peças semelhantes devem ser mostradas."

#. i18n: ectx: Menu (game)
#: src/kmahjonggui.rc:10
#, kde-format
msgid "&Game"
msgstr "Jo&go"

#. i18n: ectx: Menu (move)
#: src/kmahjonggui.rc:14
#, kde-format
msgid "&Move"
msgstr "&Movimento"

#. i18n: ectx: Menu (view)
#: src/kmahjonggui.rc:17
#, kde-format
msgid "&View"
msgstr "E&xibir"

#. i18n: ectx: ToolBar (mainToolBar)
#: src/kmahjonggui.rc:23
#, kde-format
msgid "Main Toolbar"
msgstr "Barra de ferramentas principal"

#: src/main.cpp:32
#, kde-format
msgid "KMahjongg"
msgstr "KMahjongg"

#: src/main.cpp:34
#, kde-format
msgid "Mahjongg Solitaire by KDE"
msgstr "Paciência Mahjongg do KDE"

#: src/main.cpp:36
#, kde-format
msgid ""
"(c) 1997, Mathias Mueller\n"
"(c) 2006, Mauricio Piacentini\n"
"(c) 2011, Christian Krippendorf"
msgstr ""
"(c) 1997, Mathias Mueller\n"
"(c) 2006, Mauricio Piacentini\n"
"(c) 2011, Christian Krippendorf"

#: src/main.cpp:39
#, kde-format
msgid "Mathias Mueller"
msgstr "Mathias Mueller"

#: src/main.cpp:39
#, kde-format
msgid "Original Author"
msgstr "Autor original"

#: src/main.cpp:40
#, kde-format
msgid "Christian Krippendorf"
msgstr "Christian Krippendorf"

#: src/main.cpp:40
#, kde-format
msgid "Current maintainer"
msgstr "Mantenedor atual"

#: src/main.cpp:41
#, kde-format
msgid "Albert Astals Cid"
msgstr "Albert Astals Cid"

#: src/main.cpp:41
#, kde-format
msgid "Bug fixes"
msgstr "Correções de erros"

#: src/main.cpp:42
#, kde-format
msgid "David Black"
msgstr "David Black"

#: src/main.cpp:42
#, kde-format
msgid "KDE 3 rewrite and Extension"
msgstr "Extensões e alterações para KDE 3"

#: src/main.cpp:43
#, kde-format
msgid "Michael Haertjens"
msgstr "Michael Haertjens"

#: src/main.cpp:43
#, kde-format
msgid ""
"Solvable game generation\n"
"based on algorithm by Michael Meeks in GNOME mahjongg"
msgstr ""
"Geração de jogo com garantia de solução\n"
"baseada no algoritmo feito por Michael Meeks para o mahjongg do GNOME"

#: src/main.cpp:44
#, kde-format
msgid "Raquel Ravanini"
msgstr "Raquel Ravanini"

#: src/main.cpp:44
#, kde-format
msgid "SVG Tileset for KDE4"
msgstr "Conjunto de peças em SVG para o KDE4"

#: src/main.cpp:45
#, kde-format
msgid "Richard Lohman"
msgstr "Richard Lohman"

#: src/main.cpp:45
#, kde-format
msgid "Tile set contributor and current web page maintainer"
msgstr "Colaborador do conjunto de peças e mantenedor atual do site"

#: src/main.cpp:46
#, kde-format
msgid "Osvaldo Stark"
msgstr "Osvaldo Stark"

#: src/main.cpp:46
#, kde-format
msgid "Tile set contributor and original web page maintainer"
msgstr "Colaborador do conjunto de peças e mantenedor original do site"

#: src/main.cpp:47
#, kde-format
msgid "Benjamin Meyer"
msgstr "Benjamin Meyer"

#: src/main.cpp:47
#, kde-format
msgid "Code cleanup"
msgstr "Limpeza do código"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ShowMatchingTiles)
#: src/settings.ui:22
#, kde-format
msgid "Blink matching tiles when first one is selected"
msgstr "Piscar peças semelhantes quando a primeira é selecionada"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_SolvableGames)
#: src/settings.ui:29
#, kde-format
msgid "Generate solvable games"
msgstr "Gerar jogos com solução possível"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_RemovedTiles)
#: src/settings.ui:36
#, kde-format
msgid "Show removed tiles"
msgstr "Mostrar peças removidas"

#~ msgid "You have won!"
#~ msgstr "Você venceu!"
